FROM python:3.7-alpine
LABEL maintainer="imlonghao <dockerfile@esd.cc>"
WORKDIR /app
COPY checker.py ./
RUN pip install --no-cache-dir boto3
ENTRYPOINT [ "/app/checker.py" ]