#!/usr/bin/env python3

import time
import boto3
import socket
import logging
from os import environ

AWS_ACCESS_KEY_ID = environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = environ.get('AWS_SECRET_ACCESS_KEY')
HOSTEDZONEID = environ.get('HOSTEDZONEID')
ENABLED_DOMAIN = environ.get('ENABLED_DOMAIN')
DISABLED_DOMAIN = environ.get('DISABLED_DOMAIN')
PORT = int(environ.get('PORT'))
SLEEP_TIME = int(environ.get('SLEEP_TIME'))


def if_port_opened(host, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex((host, port)) == 0


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

client = boto3.client(
    service_name='route53',
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

while True:
    time.sleep(SLEEP_TIME)

    records = client.list_resource_record_sets(
        HostedZoneId=HOSTEDZONEID).get('ResourceRecordSets')
    enabled_records = [x for x in records if x.get('Name') == ENABLED_DOMAIN]
    disabled_records = [x for x in records if x.get('Name') == DISABLED_DOMAIN]

    for record in enabled_records:
        ip = record.get('ResourceRecords')[0].get('Value')
        if not if_port_opened(ip, PORT):
            logger.info('Disabling host ' + ip)
            client.change_resource_record_sets(HostedZoneId=HOSTEDZONEID,
                                               ChangeBatch={'Comment': 'Disabling host ' + ip,
                                                            'Changes': [{'Action': 'DELETE', 'ResourceRecordSet': record}]})
            record['Name'] = DISABLED_DOMAIN
            client.change_resource_record_sets(HostedZoneId=HOSTEDZONEID,
                                               ChangeBatch={'Comment': 'Disabling host ' + ip,
                                                            'Changes': [{'Action': 'CREATE', 'ResourceRecordSet': record}]})
    for record in disabled_records:
        ip = record.get('ResourceRecords')[0].get('Value')
        if if_port_opened(ip, PORT):
            logger.info('Enabling host ' + ip)
            client.change_resource_record_sets(HostedZoneId=HOSTEDZONEID,
                                               ChangeBatch={'Comment': 'Enabling host ' + ip,
                                                            'Changes': [{'Action': 'DELETE', 'ResourceRecordSet': record}]})
            record['Name'] = ENABLED_DOMAIN
            client.change_resource_record_sets(HostedZoneId=HOSTEDZONEID,
                                               ChangeBatch={'Comment': 'Enabling host ' + ip,
                                                            'Changes': [{'Action': 'CREATE', 'ResourceRecordSet': record}]})
